const express = require("express");

const grpc = require("@grpc/grpc-js");
const PROTO_PATH = `${__dirname}/bot.proto`;
var protoLoader = require("@grpc/proto-loader");
require("dotenv").config();
let contexts;
const options = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
};
var packageDefinition = protoLoader.loadSync(PROTO_PATH, options);
const messagesProto = grpc.loadPackageDefinition(packageDefinition);
const dataService = require("./services/getData");
const app = express();
const cors = require("cors");
app.use(cors());

var JiraClient = require("jira-connector");
var jira = new JiraClient({
  host: process.env.JIRA_SERVICEDESK_URL,
  basic_auth: {
    email: "ashishsingh@leadschool.in",
    api_token: process.env.JIRA_SERVICEDESK_TOKEN,
  },
});
const requests = {
  class: "",
  division: "",
  subject: "",
};
/**
 * TODO(developer): UPDATE these variables before running the sample.
 */
//  projectId:
const projectId = process.env.GOOGLE_CLOUD_PROJECT;
// sessionId: String representing a random number or hashed user identifier
const sessionId = "123456";
// queries: A set of sequential queries to be send to Dialogflow agent for Intent Detection
// const queries = [
// 'my school is akkalkot',
// 'school is MPS', // Tell the bot when the meeting is taking place
// 'hi'  // Rooms are defined on the Dialogflow agent, default options are A, B, or C
// ]
// languageCode: Indicates the language Dialogflow agent should use to detect intents
const languageCode = "en";

// Imports the Dialogflow library
const dialogflow = require("@google-cloud/dialogflow");

// Instantiates a session client
const sessionClient = new dialogflow.SessionsClient();
const server = new grpc.Server();

server.addService(messagesProto.PostMessageService.service, {
  postMessages: async (_, callback) => {
    const intentResponse = await processMessage(_.request);
    callback(null, intentResponse);
  },
});

const sessionPath = sessionClient.projectAgentSessionPath(projectId, sessionId);
async function processMessage(requestParams) {
  try {
    const response = await detectIntent(
      projectId,
      sessionId,
      requestParams.message,
      contexts,
      languageCode
    );
    // implement processing after intent is detected here
    const userData = await dataService.getUserdata(requestParams.mobile);
    // console.log('************',userData);

    console.log(
      "response.response.queryResult.intent.displayName",
      response.response.queryResult.parameters.fields,
      response.response.queryResult.intent.displayName
    );
    const message = await getResponsePerIntent(
      response.response.queryResult.intent.displayName,
      requestParams.mobile,
      userData,
      requestParams.message,
      response.response.queryResult.parameters.fields
    );
    console.log("message----", message);
    return { message: message, fulfillmentText: response.fulfillmentText };
  } catch (err) {
    console.log(err);
    return {
      message: "",
    };
    throw err;
  }
}

server.bindAsync(
  "10.0.10.181:5000",
  grpc.ServerCredentials.createInsecure(),
  (error, port) => {
    console.log("Server running at localhost:5000");
    server.start();
  }
);

async function detectIntent(
  projectId,
  sessionId,
  query,
  contexts,
  languageCode
) {
  // The path to identify the agent that owns the created intent.
  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: query,
        languageCode: languageCode,
      },
    },
  };
  if (contexts && contexts.length > 0) {
    request.queryParams = {
      contexts: contexts,
    };
  }
  const responses = await sessionClient.detectIntent(request);
  contexts =
    responses.length > 0 ? responses[0].queryResult.outputContexts : {};
  return {
    response: responses[0],
    fulfillmentText:
      responses.length > 0 ? responses[0].queryResult.fulfillmentText : "",
  };
}

async function getResponsePerIntent(
  intentName = "",
  mobile,
  userData,
  message,
  intentFields
) {
  switch (intentName.toLowerCase()) {
    case "Default Welcome Intent".toLowerCase():
      return userData.length > 0? `Hi ${userData[0].name}, I am LEADwinder, I am here to help you. Please select what you are looking for? Raise Pause Request,Raise Skip Request, Ticket ID for status`:'';
    case "PausingAClass".toLowerCase():
      const classes = dataService.extractClasses(userData);
      const classList = classes.join(",");
      return `Please select classes from the list ${classList}`;
    case "SelectingClass".toLowerCase():
      requests.class = message;
      const divisions = dataService.extractDivisions(userData, message);
      const divList = divisions.join(",");
      return `Please select division from the list ${divList}`;
    case "SelectDivision".toLowerCase():
      requests.division = message;
      return ``;
    case "SelectingSubject".toLowerCase():
      requests.subject = message;
      return `Pause request for class ${requests.class} - division ${requests.division} - subject ${requests.subject}, Please confirm/cancel`;
    case "ConfirmTheRequest".toLowerCase():
      const issueCreated = await createJiraTicket(
        userData,
        message
      );
      return `Ticket Created with id ${issueCreated.id} and key ${issueCreated.key}, keep the key for further reference.Please select the option you need help with, Raise Pause Request,Raise Skip`;
    case "StatusOfTheTicket".toLowerCase():
      const ticket_key = intentFields.ticket ? `${intentFields.ticket.stringValue}-${intentFields['number-integer'].numberValue}`:''
      return ticket_key?await getJiraTicketStatus(ticket_key):'';
    default:
      return "";
  }
}

async function createJiraTicket(userData, message) {
  const summary = `User ${userData[0].name} requested Pausing for class ${requests.class} - division ${requests.division} - subject ${requests.subject}`;
  const issues = await jira.issue.createIssue({
    fields: {
      project: {
        id: "10091",
      },
      summary: summary,
      issuetype: {
        id: "10137",
      },
    },
  });
  return issues;
}

async function getJiraTicketStatus(issueKey) {
  try{
    const issues = await jira.issue.getIssue({ issueKey: issueKey });
    return issues && issues.fields && issues.fields.status
      ? issues.fields.status.name
      : "Unable to find ticket";
  }catch(error){
    return `issue does not exist`;
  }
}

