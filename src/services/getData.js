const knex = require("knex");
var db = knex({
  client: "pg",
  connection: {
    host: "10.0.66.226",
    port: "5432",
    user: "dev_ls_root",
    password: 'Uk@zV+kc<%9(-q5"',
    database: "ls_home",
  },
});

async function getUserdata(mobile) {
  const result = await db
    .select(
      "school_users.name",
      "school_users.mobile",
      "subjects.class_id",
      "classes.class_type",
      "classes.class_code",
      "classes.name AS class_name",
      "divisions.name AS division_name"
    )
    .from("school_subjects AS subjects")
    .join("classes", "classes.id", "subjects.class_id")
    .join("school_users", "school_users.id", "subjects.subject_teacher_id")
    .join("school_classes", "school_classes.class_id", "subjects.class_id")
    .join("divisions", "divisions.id", "school_classes.division_id")
    .where("school_users.mobile", mobile)
    .orderBy("subjects.class_id")
    .distinct();
  return result;
}
function extractClasses(userData){
  let extractedClasses = userData.map((x)=>x.class_name);
  extractedClasses=[...new Set(extractedClasses)]
  return extractedClasses;
}
function extractDivisions(userData,class_name){
  const filterClasses = userData.filter((x)=>x.class_name.toLowerCase()===class_name.toLowerCase());
  let filterDivisions = filterClasses.map((x)=>x.division_name)
  filterDivisions=[...new Set(filterDivisions)]
  return filterDivisions;
}
module.exports = { getUserdata,extractClasses,extractDivisions };
